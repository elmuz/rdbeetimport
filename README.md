# README #

This script imports a list of songs in Rivendell and fulfills all metadata by querying beets database. It expects 'beets' and 'rdimport' being installed.
Two parameters are needed. First one is Rivendell group where you want songs to be imported. Second one is the query to be piped to beets.
Usage example: rdbeetimport.py GOLD 'artist:beatles'