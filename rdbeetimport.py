#!/usr/bin/python

import sys

from subprocess import Popen, PIPE

#print('Number of arguments:', len(sys.argv), 'arguments.')
#print('Argument List:', str(sys.argv))

""" Check that there is at least one parameter """
if len(sys.argv) != 3:
	print("ERROR: Please provide Rivendell Group and *one* query string enclosed by quotes")
	print("Example: rdbeetimport.py GOLD 'artist:beatles'")
	sys.exit()

rdgroup = sys.argv[1]

""" Ingest the first parameter as beets query search """
beetstring = "beet list -f '$title ~~~ $artist ~~~ $album ~~~ $year ~~~ $label ~~~ $mb_trackid ~~~ $path' " + sys.argv[2]
process = Popen(beetstring, universal_newlines=True, shell=True, stdout=PIPE)
(output, err) = process.communicate()

list = output.splitlines()

print(str(len(list)) + " songs will be imported...\n\n")
counter = 1

for line in list:
	song = line.split(' ~~~ ')
	# [0] = title
	# [1] = artist
	# [2] = album
	# [3] = year
	# [4] = label
	# [5] = mb_trackid
	# [6] = filepath

	print("Importing song [" + str(counter) + "/" + str(len(list)) + "]")
	print("Title: " + song[0])
	print("Artist: " + song[1])
	print("Album: " + song[2])
	print("Year: " + song[3])
	print("Label: " + song[4])
	print("MB ID: " + song[5])
	print("Path: " + song[6])

	rdimportstring = ["echo", " --verbose",
	("--set-string-title=\"" + song[0] + "\""),
	("--set-string-artist=\"" + song[1] + "\""),
	("--set-string-album=\"" + song[2] + "\""),
	("--set-string-year=\"" + song[3] + "\""),
	("--set-string-label=\"" + song[4] + "\""),
	("--set-string-user-defined=\"" + song[5] + "\""),
	rdgroup, "\"", song[6], "\""]
	process = Popen(rdimportstring, universal_newlines=True)
	(output, err) = process.communicate()

	counter += 1
